import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';
import baserUrl from './base';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http:HttpClient) { }

  public agregarUsuario(user:User):Observable<any>{
    return this.http.post<User>(`${baserUrl}/usuarios/`,user)
  }
}
