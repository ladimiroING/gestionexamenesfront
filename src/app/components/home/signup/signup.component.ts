import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/user';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUpComponent implements OnInit {

  // public user = {
  //   username : '',
  //   password : '',
  //   nombre : '',
  //   apellido : '',
  //   email : '',
  //   telefono : ''
  // }

  user:User=new User();

  constructor(private snack:MatSnackBar, 
    private userSvc:UsuarioService,
    private router:Router) 
    { }

  ngOnInit(): void {
  }

  formSubmit(){
    console.log(this.user);
    if(this.user.username == '' || this.user.username == null){
      this.snack.open('El nombre de usuario es requerido !!','Aceptar',{
        duration : 3000,
        verticalPosition : 'top',
        horizontalPosition : 'right'
      });
      return;
    }
    this.userSvc.agregarUsuario(this.user).subscribe(data=>{
      this.user=data
      console.log(this.user);
      Swal.fire('Usuario guardado','Usuario registrado con exito en el sistema','success');
      this.router.navigate(['login'])
    },err=>{
      console.log(err);
      this.snack.open('Ha ocurrido un error en el sistema !!','Aceptar',{
        duration : 3000
      });
    }
      
    )
  }
}
