import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { LoginData } from 'src/app/interfaces/logindata';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData = {
    "username" : '',
    "password" : '',
  }

 //loginData:LoginData=new LoginData()

  constructor(private snack:MatSnackBar, 
              private loginSvc:LoginService,
              private router:Router) 
  { }

  ngOnInit(): void {
  }

  

  formSubmit(){
    if(this.loginData.username.trim() == '' || this.loginData.username.trim() == null){
      this.snack.open('El nombre de usuario es requerido !!','Aceptar',{
        duration:3000
      })
      return;
    }

    if(this.loginData.password.trim() == '' || this.loginData.password.trim() == null){
      this.snack.open('La contraseña es requerida !!','Aceptar',{
        duration:3000
      })
      return;
    }

    this.loginSvc.generateToken(this.loginData).subscribe(data=>{
      console.log(data);
      this.loginSvc.loginUser(data.token);
      this.loginSvc.getCurrentUser().subscribe(res=>{
        this.loginSvc.setUser(res);
        console.log(res)

        if(this.loginSvc.getUserRole() == 'ADMIN'){
          //dashboard admin
          //window.location.href = '/dashboard';
           this.router.navigate(['dashboard/admin']);
           this.loginSvc.loginStatusSubjec.next(true);
        }
        else if(this.loginSvc.getUserRole() == 'ESTUDIANTE'){
          //user dashboard
          //window.location.href = '/dashboard';
           this.router.navigate(['dashboard/user/0']);
           this.loginSvc.loginStatusSubjec.next(true);
        }
        else{
          this.loginSvc.logout();
        }
      })
    },err=>{
      console.log(err);
      this.snack.open('Detalles inválidos , vuelva a intentar !!','Aceptar',{
        duration:3000
      })
    })
  }
  
  

}
