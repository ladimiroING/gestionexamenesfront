import { Component, OnInit } from '@angular/core';
import { CategoriaService } from 'src/app/services/categoria.service';

@Component({
  selector: 'app-view-categorias',
  templateUrl: './view-categorias.component.html',
  styleUrls: ['./view-categorias.component.css']
})
export class ViewCategoriasComponent implements OnInit {

  categorias:any=[

  ]

  constructor(private categoriasSvc:CategoriaService) { }

  ngOnInit(): void {
    this.listarCategorias();
  }

  listarCategorias(){
    this.categoriasSvc.listarCategorias().subscribe(data=>{
      console.log(data);
      this.categorias=data
    },err=>{
      console.log(err);
    })
  }

}
