import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { LoginComponent } from '../../home/login/login.component';
import { NavbarComponent } from '../../home/navbar/navbar.component';
import { SignUpComponent } from '../../home/signup/signup.component';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { MaterialModules } from '../material.module';
import { FormsModule } from '@angular/forms';
import { InicioComponent } from '../../home/inicio/inicio.component';
import { authInterceptorProviders } from 'src/app/services/authinterceptor';
import { UserComponent } from '../../pages/user/user.component';
import { AdminComponent } from '../../pages/admin/admin.component';
import { PerfilComponent } from '../../pages/admin/perfil/perfil.component';
import { SidebarComponent } from '../../pages/admin/sidebar/sidebar.component';
import { WelcomeComponent } from '../../pages/admin/welcome/welcome.component';
import { ViewCategoriasComponent } from '../../pages/admin/view-categorias/view-categorias.component';
import { AddCategoriasComponent } from '../../pages/admin/add-categorias/add-categorias.component';
import { ViewExamenesComponent } from '../../pages/admin/view-examenes/view-examenes.component';
import { AddExamenesComponent } from '../../pages/admin/add-examenes/add-examenes.component';
import { ActualizarExamenComponent } from '../../pages/admin/actualizar-examen/actualizar-examen.component';
import { ViewExamenPreguntasComponent } from '../../pages/admin/view-examen-preguntas/view-examen-preguntas.component';
import { AddPreguntaComponent } from '../../pages/admin/add-pregunta/add-pregunta.component';
import { ActualizarPreguntaComponent } from '../../pages/admin/actualizar-pregunta/actualizar-pregunta.component';
import { SidebarComponent as UserSideBar} from '../../pages/user/sidebar/sidebar.component';
import { LoadExamenComponent } from '../../pages/user/load-examen/load-examen.component';
import { InstruccionesComponent } from '../../pages/user/instrucciones/instrucciones.component';
import { StartExamenComponent } from '../../pages/user/start-examen/start-examen.component';


@NgModule({
  declarations: [
    LoginComponent,
    NavbarComponent,
    SignUpComponent,
    InicioComponent,
    DashboardComponent,
    UserComponent,
    AdminComponent,
    PerfilComponent,
    SidebarComponent,
    WelcomeComponent,
    ViewCategoriasComponent,
    AddCategoriasComponent,
    ViewExamenesComponent,
    AddExamenesComponent,
    ActualizarExamenComponent,
    ViewExamenPreguntasComponent,
    AddPreguntaComponent,
    ActualizarPreguntaComponent,
    UserSideBar,
    LoadExamenComponent,
    InstruccionesComponent,
    StartExamenComponent
  ],
  exports:[
    LoginComponent,
    NavbarComponent,
    SignUpComponent,
    InicioComponent,
    DashboardComponent,
    UserComponent,
    AdminComponent,
    PerfilComponent,
    SidebarComponent,
    WelcomeComponent,
    ViewCategoriasComponent,
    AddCategoriasComponent,
    ViewExamenesComponent,
    AddExamenesComponent,
    ActualizarExamenComponent,
    ViewExamenPreguntasComponent,
    AddPreguntaComponent,
    ActualizarPreguntaComponent,
    UserSideBar,
    LoadExamenComponent,
    InstruccionesComponent,
    StartExamenComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    MaterialModules,
    FormsModule
    
  ],
  providers:[authInterceptorProviders]
})
export class PagesModule { }
