import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from 'src/app/services/admin.guard';
import { UserGuard } from 'src/app/services/user.guard';
import { AdminComponent } from '../../pages/admin/admin.component';
import { WelcomeComponent } from '../../pages/admin/welcome/welcome.component';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { PerfilComponent } from '../../pages/admin/perfil/perfil.component';
import { UserComponent } from '../../pages/user/user.component';
import { ViewCategoriasComponent } from '../../pages/admin/view-categorias/view-categorias.component';
import { AddCategoriasComponent } from '../../pages/admin/add-categorias/add-categorias.component';
import { ViewExamenesComponent } from '../../pages/admin/view-examenes/view-examenes.component';
import { AddExamenesComponent } from '../../pages/admin/add-examenes/add-examenes.component';
import { ActualizarExamenComponent } from '../../pages/admin/actualizar-examen/actualizar-examen.component';
import { ViewExamenPreguntasComponent } from '../../pages/admin/view-examen-preguntas/view-examen-preguntas.component';
import { AddPreguntaComponent } from '../../pages/admin/add-pregunta/add-pregunta.component';
import { ActualizarPreguntaComponent } from '../../pages/admin/actualizar-pregunta/actualizar-pregunta.component';
import { LoadExamenComponent } from '../../pages/user/load-examen/load-examen.component';
import { InstruccionesComponent } from '../../pages/user/instrucciones/instrucciones.component';
import { StartExamenComponent } from '../../pages/user/start-examen/start-examen.component';

const routes: Routes = [
  {path:'',component:DashboardComponent,children:[
    {path:'admin',component:AdminComponent,canActivate:[AdminGuard],
    children:[
      {path:'',component:WelcomeComponent},
      {path:'perfil',component:PerfilComponent},
      {path:'categorias',component:ViewCategoriasComponent},
      {path:'agregar-categoria',component:AddCategoriasComponent},
      {path:'examenes',component:ViewExamenesComponent},
      {path:'add-examen',component:AddExamenesComponent},
      {path:'examen/:examenId',component:ActualizarExamenComponent},
      {path:'ver-preguntas/:examenId/:titulo',component:ViewExamenPreguntasComponent},
      {path:'add-pregunta/:examenId/:titulo',component:AddPreguntaComponent},
      {path:'pregunta/:preguntaId',component:ActualizarPreguntaComponent}
    ]
    },
    {path:'user',component:UserComponent,canActivate:[UserGuard],
    children:[
      {path:':catId',component:LoadExamenComponent},
      {path:'instrucciones/:examenId',component:InstruccionesComponent}
    ]  
    },
    {path:'start/:examenId',component:StartExamenComponent,canActivate:[UserGuard]}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
